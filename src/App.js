
import { Container } from 'react-bootstrap';

import './App.css';
import AppNavBar from './components/AppNavBar';
import Home from './pages/Home'
/*
  All other components/pages will be contained in our main component: <App />
*/

function App() {
  return (
    <>
      <AppNavBar />
      <Container fluid>
        <Home />
      </Container>
    </>
  );
}

export default App;
