import {Row, Col, Card, Button} from 'react-bootstrap';

export default function Course(){
    return(
        <Row className="my-3">
            <Col xs={12} md={12}>
                <Card>
                    <Card.Body>
                        <Card.Title>
                            <h4>Sample Course</h4>
                        </Card.Title>
                        <Card.Subtitle className="mb-0">
                            Description:
                        </Card.Subtitle>
                        <Card.Text className="mt-0">
                            This is a sample course offering.
                        </Card.Text>
                        <Card.Subtitle className="mb-0">
                            Price:
                        </Card.Subtitle>
                        <Card.Text className="mt-0">
                            PhP 40,000
                        </Card.Text>
                        <Button variant="primary">
                            Enroll!
                        </Button>
                    </Card.Body>
                </Card>
            </Col>
        </Row>
        );
}

